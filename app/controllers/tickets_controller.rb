class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]

  def index
    @tickets = Ticket.all #Ticket.include(excavators).all
  end

  def create
    @ticket = Ticket.new(ticket_params)
    @excavator = @ticket.excavators.new(excavator_params)
    respond_to do |format|
      if @ticket.save && @excavator.save
        format.html { redirect_to @ticket, notice: 'Ticket was successfully created.' }
        format.json { render :show, status: :created, location: @ticket }
      else
        format.html { render :new }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end


  private
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    def ticket_params
      params.require(:ticket).permit(:request_number, :sequence_number, :request_type, :request_taken, :transmission_date_time, :legal, :response_due, :restake_on, :expiration_on, :primary_service_area_code, :additional_service_area_codes, :digsiteInfo)
    end

      def excavator_params
      params.require(:excavator).permit(:ticket_id, :company_name, :address, :crew_on_site)
    end
end
