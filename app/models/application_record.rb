class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  require 'rgeo/geo_json'
end
