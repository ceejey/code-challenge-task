# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Ticket.create(request_number: '09252012-00001',
			  sequence_number: '2421',
			  request_type: 'Normal',
			  request_taken: 'Luke',
			  transmission_date_time: DateTime.now,
			  legal: DateTime.now+5.days,
			  response_due: DateTime.now+10.days,
			  restake_on: DateTime.now+15.days,
			  expiration_on: DateTime.now+20.days,
			  primary_service_area_code: 'ZZGL103',
			  additional_service_area_codes: ["ZZL01","ZZL02","ZZL03"],
			  digsiteInfo: "POLYGON((-81.13390268058475 32.07206917625161,-81.14660562247929 32.04064386441295,-81.08858407706913
32.02259853170128,-81.05322183341679 32.02434500961698,-81.05047525138554 32.042681017283066,-81.0319358226746 32.06537765335268,-81.01202310294804
32.078469305179404,-81.02850259513554 32.07963291684719,-81.07759774894413 32.07090546831167,-81.12154306144413 32.08806865844325,-81.13390268058475
32.07206917625161))" )

Excavator.create(ticket_id: 1, company_name: 'Octopus Technologies', address: 'Bellandur post, Bangalore, India, 580103', crew_on_site: true)
Excavator.create(ticket_id: 1, company_name: 'Odessa Technologies', address: 'Bellandur Gate, Bangalore, India, 580103', crew_on_site: false)