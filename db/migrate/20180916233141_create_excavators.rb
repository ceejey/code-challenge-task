class CreateExcavators < ActiveRecord::Migration[5.2]
  def change
    create_table :excavators do |t|
      t.references :ticket, foreign_key: true
      t.string :company_name
      t.text :address
      t.boolean :crew_on_site

      t.timestamps
    end
  end
end
