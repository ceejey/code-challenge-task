require 'rails_helper'

RSpec.describe Ticket, type: :model do

  it 'has a valid factory' do
    expect(FactoryGirl.create(:ticket)).to be_valid
  end

  context 'validations' do
    it { is_expected.to validate_presence_of :request_number }
    it { is_expected.to validate_uniqueness_of :sequence_number }
    it { is_expected.to validate_confirmation_of :password }
    it { is_expected.to validate_presence_of :primary_service_area_code }
    it { is_expected.to validate_presence_of :additional_service_area_codes }
    it { is_expected.to validate_presence_of :digsiteInfo }
  end
end