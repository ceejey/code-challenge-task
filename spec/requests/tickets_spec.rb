# /v1/tickets if maintaining version

describe 'GET /tickets' do
  let!(:users) { FactoryGirl.create_list(:user, 10) }

  before { get '/tickets', headers: { 'Accept': 'application/vnd' } }

  it 'returns HTTP status 200' do
    expect(response).to have_http_status 200
  end

  it 'returns all users' do
    body = JSON.parse(response.body)
    expect(body['data'].size).to eq(10)
  end
end