# Add additional requires below this line. Rails is not loaded until this point!
require 'shoulda/matchers'
require 'support/shoulda'
require 'support/database_cleaner'
require 'support/request_helpers'

config.include Request::JsonHelpers, type: :request