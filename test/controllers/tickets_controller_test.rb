require 'test_helper'

class TicketsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ticket = tickets(:one)
  end

  test "should get index" do
    get tickets_url
    assert_response :success
  end

  test "should get new" do
    get new_ticket_url
    assert_response :success
  end

  test "should create ticket" do
    assert_difference('Ticket.count') do
      post tickets_url, params: { ticket: { additional_service_area_codes: @ticket.additional_service_area_codes, expiration_on: @ticket.expiration_on, legal: @ticket.legal, primary_service_area_code: @ticket.primary_service_area_code, request_number: @ticket.request_number, request_taken: @ticket.request_taken, request_type: @ticket.request_type, response_due: @ticket.response_due, restake_on: @ticket.restake_on, sequence_number: @ticket.sequence_number, transmission_date_time: @ticket.transmission_date_time } }
    end

    assert_redirected_to ticket_url(Ticket.last)
  end

  test "should show ticket" do
    get ticket_url(@ticket)
    assert_response :success
  end

  test "should get edit" do
    get edit_ticket_url(@ticket)
    assert_response :success
  end

  test "should update ticket" do
    patch ticket_url(@ticket), params: { ticket: { additional_service_area_codes: @ticket.additional_service_area_codes, expiration_on: @ticket.expiration_on, legal: @ticket.legal, primary_service_area_code: @ticket.primary_service_area_code, request_number: @ticket.request_number, request_taken: @ticket.request_taken, request_type: @ticket.request_type, response_due: @ticket.response_due, restake_on: @ticket.restake_on, sequence_number: @ticket.sequence_number, transmission_date_time: @ticket.transmission_date_time } }
    assert_redirected_to ticket_url(@ticket)
  end

  test "should destroy ticket" do
    assert_difference('Ticket.count', -1) do
      delete ticket_url(@ticket)
    end

    assert_redirected_to tickets_url
  end
end
