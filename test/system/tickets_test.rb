require "application_system_test_case"

class TicketsTest < ApplicationSystemTestCase
  setup do
    @ticket = tickets(:one)
  end

  test "visiting the index" do
    visit tickets_url
    assert_selector "h1", text: "Tickets"
  end

  test "creating a Ticket" do
    visit tickets_url
    click_on "New Ticket"

    fill_in "Additional Service Area Codes", with: @ticket.additional_service_area_codes
    fill_in "Expiration On", with: @ticket.expiration_on
    fill_in "Legal", with: @ticket.legal
    fill_in "Primary Service Area Code", with: @ticket.primary_service_area_code
    fill_in "Request Number", with: @ticket.request_number
    fill_in "Request Taken", with: @ticket.request_taken
    fill_in "Request Type", with: @ticket.request_type
    fill_in "Response Due", with: @ticket.response_due
    fill_in "Restake On", with: @ticket.restake_on
    fill_in "Sequence Number", with: @ticket.sequence_number
    fill_in "Transmission Date Time", with: @ticket.transmission_date_time
    click_on "Create Ticket"

    assert_text "Ticket was successfully created"
    click_on "Back"
  end

  test "updating a Ticket" do
    visit tickets_url
    click_on "Edit", match: :first

    fill_in "Additional Service Area Codes", with: @ticket.additional_service_area_codes
    fill_in "Expiration On", with: @ticket.expiration_on
    fill_in "Legal", with: @ticket.legal
    fill_in "Primary Service Area Code", with: @ticket.primary_service_area_code
    fill_in "Request Number", with: @ticket.request_number
    fill_in "Request Taken", with: @ticket.request_taken
    fill_in "Request Type", with: @ticket.request_type
    fill_in "Response Due", with: @ticket.response_due
    fill_in "Restake On", with: @ticket.restake_on
    fill_in "Sequence Number", with: @ticket.sequence_number
    fill_in "Transmission Date Time", with: @ticket.transmission_date_time
    click_on "Update Ticket"

    assert_text "Ticket was successfully updated"
    click_on "Back"
  end

  test "destroying a Ticket" do
    visit tickets_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ticket was successfully destroyed"
  end
end
